import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';

@IonicPage()
@Component({
  selector: 'page-over-speed-repo',
  templateUrl: 'over-speed-repo.html',
})
export class OverSpeedRepoPage implements OnInit {
  overSpeeddevice_id: any = [];
  overspeedReport: any[] = [];
  // Ignitiondevice_id: any;
  datetimeEnd: string;
  datetimeStart: string;
  islogin: any;
  devices1243: any[];
  devices: any;
  portstemp: any;
  isdevice: string;
  datetime: number;
  selectedVehicle: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");

  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalloverspeed: ApiServiceProvider, public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    console.log('start date', this.datetimeStart)
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
    console.log('stop date', this.datetimeEnd);
  }

  ngOnInit() {
    this.getdevices();
  }

  change(datetimeStart) {
    console.log(datetimeStart);
  }

  change1(datetimeEnd) {
    console.log(datetimeEnd);
  }

  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.overspeedReport);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
    self.file.writeFile(self.file.dataDirectory, "overspeed_report_download.xlsx", blob, { replace: true })
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  changeDate(key) {
    this.datetimeStart = undefined;
    if (key === 'today') {
      this.datetimeStart = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeStart = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeStart = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeStart = moment().startOf('month').format();
    }
  }

  getOverspeeddevice(selectedVehicle) {
    this.overSpeeddevice_id = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.overSpeeddevice_id.push(selectedVehicle[t].Device_ID)
        }
      } else {
        this.overSpeeddevice_id.push(selectedVehicle[0].Device_ID)
      }
    } else return;
    console.log("selectedVehicle=> ", this.overSpeeddevice_id)
  }

  getdevices() {
    var baseURLp = this.apicalloverspeed.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalloverspeed.startLoading().present();
    this.apicalloverspeed.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalloverspeed.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apicalloverspeed.stopLoading();
          console.log(err);
        });
  }


  getOverSpeedReport(starttime, endtime) {
    var baseUrl;
    if (this.overSpeeddevice_id.length === 0) {
      baseUrl = this.apicalloverspeed.mainUrl + "notifs/overSpeedReport?from_date=" + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id

    } else {
      baseUrl = this.apicalloverspeed.mainUrl + "notifs/overSpeedReport?from_date=" + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id + '&device=' + this.overSpeeddevice_id
    }
    this.apicalloverspeed.startLoading().present();
    this.apicalloverspeed.getOverSpeedApi(baseUrl)
      .subscribe(data => {
        this.apicalloverspeed.stopLoading();
        // this.overspeedReport = data;
        if (data.length > 0) {
          this.innerFunc(data);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected dates/Vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
      }, error => {
        this.apicalloverspeed.stopLoading();
        console.log(error);
      })
  }

  innerFunc(overspeedReport) {
    let outerthis = this;
    var i = 0, howManyTimes = overspeedReport.length;
    function f() {

      outerthis.overspeedReport.push({
        'vehicleName': overspeedReport[i].vehicleName,
        'overSpeed': overspeedReport[i].overSpeed,
        'start_location': {
          'lat': overspeedReport[i].lat,
          'long': overspeedReport[i].long
        },
      });

      outerthis.start_address(outerthis.overspeedReport[i], i);
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }

  // start_address(item, index) {
  //   let that = this;
  //   that.overspeedReport[index].StartLocation = "N/A";
  //   if (!item.start_location) {
  //     that.overspeedReport[index].StartLocation = "N/A";
  //   } else if (item.start_location) {
  //     this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
  //       .then((res) => {
  //         console.log("test", res)
  //         var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
  //         that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
  //         that.overspeedReport[index].StartLocation = str;
  //       })
  //   }
  // }

  start_address(item, index) {
    let that = this;
    if (!item.start_location) {
      that.overspeedReport[index].StartLocation = "N/A";
      return;
    }
    let tempcord = {
      "lat": item.start_location.lat,
      "long": item.start_location.long
    }
    this.apicalloverspeed.getAddress(tempcord)
      .subscribe(res => {
        console.log("test");
        console.log("result", res);
        console.log(res.address);
        // that.allDevices[index].address = res.address;
        if (res.message == "Address not found in databse") {
          this.geocoderApi.reverseGeocode(item.start_location.lat, item.start_location.long)
            .then(res => {
              var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
              that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
              that.overspeedReport[index].StartLocation = str;
              // console.log("inside", that.address);
            })
        } else {
          that.overspeedReport[index].StartLocation = res.address;
        }
      })
  }

  saveAddressToServer(address, lat, lng) {
    let payLoad = {
      "lat": lat,
      "long": lng,
      "address": address
    }
    this.apicalloverspeed.saveGoogleAddressAPI(payLoad)
      .subscribe(respData => {
        console.log("check if address is stored in db or not? ", respData)
      },
        err => {
          console.log("getting err while trying to save the address: ", err);
        });
  }

}
