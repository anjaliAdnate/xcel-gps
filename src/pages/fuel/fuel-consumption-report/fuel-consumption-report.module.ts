import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FuelConsumptionReportPage } from './fuel-consumption-report';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';

@NgModule({
  declarations: [
    FuelConsumptionReportPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(FuelConsumptionReportPage),
    SelectSearchableModule,
    TranslateModule.forChild(),
  ],
  exports: [
    OnCreate
  ],
})
export class FuelConsumptionReportPageModule {}
